package com.example.devgrid.githubgistcommenter.utils;

import android.app.Activity;

import com.google.zxing.integration.android.IntentIntegrator;

/**
 * Created by claudio on 14/05/2018.
 */

public class QrCodeReader {
    Activity act;

    public QrCodeReader(Activity act) {
        this.act = act;
    }

    /**
     * Initiate the QrCodeScanner
     */
    public void turnOnQrCodeReader() {
        IntentIntegrator integrator = new IntentIntegrator(act);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }
}
