package com.example.devgrid.githubgistcommenter.presenter.oauth;

import com.example.devgrid.githubgistcommenter.BuildConfig;
import com.example.devgrid.githubgistcommenter.manager.GistAccessManager;
import com.example.devgrid.githubgistcommenter.manager.ServiceGenerator;
import com.example.devgrid.githubgistcommenter.model.AccessToken;
import com.example.devgrid.githubgistcommenter.presenter.BasePresenter;
import com.example.devgrid.githubgistcommenter.service.GitHubClient;
import com.example.devgrid.githubgistcommenter.view.oauth.OauthView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by claudio on 14/05/2018.
 */

public class OAuthPresenter extends BasePresenter<OauthView> implements OAuthInterface {
    private String clientID = BuildConfig.clientID;
    private String clientSecret = BuildConfig.clientSecret;
    private final static String BASE_URL_FOR_OAUTH = "https://github.com";

    public OAuthPresenter(OauthView view) {
        super(view);
    }


    /**
     * Get the access token to make future requests that must have authorization token.
     *
     * @param code oauth token tha was requested with a oauth authentication request
     */
    @Override
    public void getAccesstoken(String code) {
        getView().ahowProgressOathLoading();
        GitHubClient client = ServiceGenerator.createService(GitHubClient.class, BASE_URL_FOR_OAUTH);
        Call<AccessToken> accessTokenCall = client.getAccessToken(clientID, clientSecret, code);
        accessTokenCall.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                getView().dismissProgress();
                GistAccessManager.getInstance().setAccessToken(response.body().getAccessToken()); //save the token on the singleton Class
                getView().goToQrCodeSection();
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                getView().dismissProgress();
                // ((OauthView) getView()).ahowToastOathError();
                getView().showToastError();
            }
        });
    }

}

