package com.example.devgrid.githubgistcommenter.view.qrcodescanner;

import com.example.devgrid.githubgistcommenter.view.BaseView;

public interface QrCodeView extends BaseView {

    public void onRequestOrCodeBtnPressed();

    public void startScanningQrCode();

    public void goToGistCommentSecction();

    public void showInvalidGistToast();

    public void showProgressDialogValidateGist();


}
