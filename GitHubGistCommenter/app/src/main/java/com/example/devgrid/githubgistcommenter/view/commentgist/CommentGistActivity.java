package com.example.devgrid.githubgistcommenter.view.commentgist;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.devgrid.githubgistcommenter.R;
import com.example.devgrid.githubgistcommenter.manager.GistAccessManager;
import com.example.devgrid.githubgistcommenter.presenter.commentGist.CommentGistInterface;
import com.example.devgrid.githubgistcommenter.view.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

public class CommentGistActivity extends BaseActivity implements CommentGistView {

    @Inject
    public CommentGistInterface presenter;

    @BindView(R.id.gistID)
    TextView gistId;
    @BindView(R.id.editGistComment)
    EditText gistEdit;
    @BindView(R.id.userName)
    TextView userName;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_gist);
        ButterKnife.bind(this);
        initControls();
    }

    @OnClick(R.id.sendGistCommentBtn)
    @Override
    public void onSendCommentBtnPressed() {
        if (isNotAnEmptyComment()) {
            presenter.sendComment(gistEdit.getText().toString());
        } else {
            showToast(getResources().getString(R.string.comment_empty));
        }

    }


    @Override
    public void showToastGistCommented() {
        showToast(getResources().getString(R.string.gist_commented));
    }

    @Override
    public void showProgressCommentingGist() {
        showProgress(getResources().getString(R.string.gist_commenting));
    }

    /**
     * initiate the variables
     */
    public void initControls() {
        gistId.setText(GistAccessManager.getInstance().getTargetGist().getId());
        userName.setText(GistAccessManager.getInstance().getTargetGist().getOwner().getName());
    }

    /**
     * Check if the comment field is not empty before send the comment.
     *
     * @return true if it is not empty and false if it is empty
     */
    private boolean isNotAnEmptyComment() {
        if (!gistEdit.getText().toString().equals("")) {
            return true;
        } else {
            return false;
        }
    }
}
