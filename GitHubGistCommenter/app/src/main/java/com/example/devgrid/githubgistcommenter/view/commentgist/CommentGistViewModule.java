package com.example.devgrid.githubgistcommenter.view.commentgist;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CommentGistViewModule {
    @Binds
    abstract CommentGistView provideCommentGistView(CommentGistActivity commentGistActivity);
}
