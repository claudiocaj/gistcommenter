package com.example.devgrid.githubgistcommenter.presenter.oauth;

public interface OAuthInterface {

    public void getAccesstoken(String code);
}
