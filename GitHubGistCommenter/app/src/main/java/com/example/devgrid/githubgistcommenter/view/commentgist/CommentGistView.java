package com.example.devgrid.githubgistcommenter.view.commentgist;

import com.example.devgrid.githubgistcommenter.view.BaseView;

public interface CommentGistView extends BaseView {

    public void onSendCommentBtnPressed();

    public void showToastGistCommented();

    public void showProgressCommentingGist();
}
