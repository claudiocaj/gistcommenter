package com.example.devgrid.githubgistcommenter.view.qrcodescanner;

import android.content.Intent;
import android.os.Bundle;

import com.example.devgrid.githubgistcommenter.R;
import com.example.devgrid.githubgistcommenter.presenter.qrcodescanner.QrCodeScannerInterface;
import com.example.devgrid.githubgistcommenter.utils.QrCodeReader;
import com.example.devgrid.githubgistcommenter.view.BaseActivity;
import com.example.devgrid.githubgistcommenter.view.commentgist.CommentGistActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

public class QrCodeScannerActivity extends BaseActivity implements QrCodeView {
    @Inject
    public QrCodeScannerInterface presenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_scanner);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.readGistBtn)
    @Override
    public void onRequestOrCodeBtnPressed() {
        startScanningQrCode();
    }

    @Override
    public void startScanningQrCode() {
        QrCodeReader reader = new QrCodeReader(QrCodeScannerActivity.this);
        reader.turnOnQrCodeReader();
    }


    /**
     * Activity result for the barCode scanning
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                showToast(getResources().getString(R.string.scanning_cancelled));
            } else {
                presenter.validateGist(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * go to the gist comment section in case that the user got a valid gist by qrCode reading
     */
    public void goToGistCommentSecction() {
        Intent intent = new Intent(QrCodeScannerActivity.this, CommentGistActivity.class);
        startActivity(intent);
    }


    @Override
    public void showInvalidGistToast() {
        showToast(getResources().getString(R.string.invalid_gist));
    }

    @Override
    public void showProgressDialogValidateGist() {
        showProgress(getResources().getString(R.string.check_gist));
    }


}
