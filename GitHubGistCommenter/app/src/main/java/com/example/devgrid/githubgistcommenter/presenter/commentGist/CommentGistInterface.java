package com.example.devgrid.githubgistcommenter.presenter.commentGist;

import com.example.devgrid.githubgistcommenter.view.BaseActivity;

public interface CommentGistInterface  {
    public void sendComment(String commentString);


}
