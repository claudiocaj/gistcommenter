package com.example.devgrid.githubgistcommenter.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by claudio on 13/05/2018.
 */

public class AccessToken {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("access_type")
    private String tokenType;

    public String getAccessToken() {
        return accessToken;
    }

    public String getAccessType() {
        return tokenType;
    }
}
