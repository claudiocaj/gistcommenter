package com.example.devgrid.githubgistcommenter.di;

import android.content.Context;

import com.example.devgrid.githubgistcommenter.App;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    @Provides
    Context proviContext(App app) {
        return app.getApplicationContext();
    }
}
