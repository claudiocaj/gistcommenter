package com.example.devgrid.githubgistcommenter.view.qrcodescanner;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class QrCodeViewModule {

    @Binds
    abstract QrCodeView provideQrCodeView(QrCodeScannerActivity qrCodeScannerActivity);
}
