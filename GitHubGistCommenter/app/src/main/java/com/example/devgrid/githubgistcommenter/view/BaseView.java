package com.example.devgrid.githubgistcommenter.view;

public interface BaseView {

    public void showProgress(String msg);

    public void dismissProgress();

    public void showToast(String msg);

    public void showToastError();
}
