package com.example.devgrid.githubgistcommenter.di;

import com.example.devgrid.githubgistcommenter.App;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjection;


@Singleton
@Component(modules = {AppModule.class, BuildersModule.class})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(App app);

        AppComponent build();
    }

    void inject(App app);


}
