package com.example.devgrid.githubgistcommenter.manager;

import com.example.devgrid.githubgistcommenter.model.AccessToken;
import com.example.devgrid.githubgistcommenter.model.Gist;

import java.util.zip.GZIPInputStream;

/**
 * Created by claudio on 14/05/2018.
 */

public class GistAccessManager {
    public static GistAccessManager instance = null;
    public String gistId;
    public String accessToken;
    public Gist targetGist;

    public static GistAccessManager getInstance() {
        if (instance == null) {
            instance = new GistAccessManager();
        }
        return instance;

    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getGistId() {
        return gistId;
    }

    public void setGistId(String gistId) {
        this.gistId = gistId;
    }

    public Gist getTargetGist() {
        return targetGist;
    }

    public void setTargetGist(Gist targetGist) {
        this.targetGist = targetGist;
    }
}
