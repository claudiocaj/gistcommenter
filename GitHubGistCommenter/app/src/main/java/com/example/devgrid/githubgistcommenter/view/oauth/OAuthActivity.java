package com.example.devgrid.githubgistcommenter.view.oauth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.devgrid.githubgistcommenter.BuildConfig;
import com.example.devgrid.githubgistcommenter.R;
import com.example.devgrid.githubgistcommenter.presenter.oauth.OAuthInterface;
import com.example.devgrid.githubgistcommenter.view.BaseActivity;
import com.example.devgrid.githubgistcommenter.view.qrcodescanner.QrCodeScannerActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

public class OAuthActivity extends BaseActivity implements OauthView {
    @Inject
    public OAuthInterface presenter;

    private String clientID = BuildConfig.clientID;
    private String redirectUri = BuildConfig.redirectUI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.requestOAuthBtn)
    @Override
    public void onRequestOAuthBtnPressed() {
        requestAuthorizationAuth();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Uri uri = getIntent().getData();
        if (uri != null) {
            presenter.getAccesstoken(uri.getQueryParameter("code"));
        }
    }

    @Override
    public void ahowProgressOathLoading() {
        super.showProgress(getResources().getString(R.string.loading));
    }

    /**
     * request the oauthAuthorazition
     */
    private void requestAuthorizationAuth() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/login/oauth/authorize" + "?client_id=" + clientID + "&scope=gist&redirect_uri=" + redirectUri));
        startActivity(intent);
    }

    /**
     * After validate the oauth, this function makes the user go to the QrCode Scanning section
     */
    @Override
    public void goToQrCodeSection() {
        Intent intent = new Intent(OAuthActivity.this, QrCodeScannerActivity.class);
        startActivity(intent);
    }

}
