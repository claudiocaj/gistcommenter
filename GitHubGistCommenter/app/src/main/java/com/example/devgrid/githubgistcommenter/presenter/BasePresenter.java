package com.example.devgrid.githubgistcommenter.presenter;

import android.view.View;

import com.example.devgrid.githubgistcommenter.view.BaseActivity;

public abstract class BasePresenter<V> {
    protected final V view;

    public BasePresenter(V view) {
        this.view = view;
    }

    protected V getView() {
        return view;
    }
}
