package com.example.devgrid.githubgistcommenter.service;

import com.example.devgrid.githubgistcommenter.model.AccessToken;
import com.example.devgrid.githubgistcommenter.model.Gist;
import com.example.devgrid.githubgistcommenter.model.GistComment;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by claudio on 13/05/2018.
 */

public interface GitHubClient {

    @Headers("Accept: application/json")
    @POST("/login/oauth/access_token")
    @FormUrlEncoded
    Call<AccessToken> getAccessToken(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("code") String code
    );

    @GET("/gists/{gist_id}")
    Call<Gist> validateGistt(@Path("gist_id") String gist);

    @Headers("Accept: application/json")
    @POST("/gists/{gist_id}/comments")
    Call<ResponseBody> commentAGist(@Path("gist_id") String id, @Body GistComment comment, @Header("Authorization") String authorizationToken);


}
