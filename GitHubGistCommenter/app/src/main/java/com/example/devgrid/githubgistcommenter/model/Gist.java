package com.example.devgrid.githubgistcommenter.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by claudio on 15/05/2018.
 */

public class Gist {
    @SerializedName("owner")
    public GistOwner owner;

    @SerializedName("id")
    public String id;

    public GistOwner getOwner() {
        return owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
