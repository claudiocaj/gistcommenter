package com.example.devgrid.githubgistcommenter.view.commentgist;

import com.example.devgrid.githubgistcommenter.presenter.commentGist.CommentGistInterface;
import com.example.devgrid.githubgistcommenter.presenter.commentGist.CommentGistPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class CommentGistModule {
    @Provides
    CommentGistInterface provideCommentGistPresenter(CommentGistView commentGistView) {
        return new CommentGistPresenter(commentGistView);
    }
}
