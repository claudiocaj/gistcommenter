package com.example.devgrid.githubgistcommenter.model;

/**
 * Created by claudio on 13/05/2018.
 */

public class GistComment {

    public String body;

    public GistComment(String body) {
        this.body = body;
    }
}
