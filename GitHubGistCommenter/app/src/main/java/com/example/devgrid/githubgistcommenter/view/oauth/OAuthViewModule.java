package com.example.devgrid.githubgistcommenter.view.oauth;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class OAuthViewModule {
    @Binds
    abstract OauthView provideOAuthView(OAuthActivity oAuthActivity);
}
