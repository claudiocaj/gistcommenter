package com.example.devgrid.githubgistcommenter.presenter.qrcodescanner;

import com.example.devgrid.githubgistcommenter.manager.GistAccessManager;
import com.example.devgrid.githubgistcommenter.manager.ServiceGenerator;
import com.example.devgrid.githubgistcommenter.model.Gist;
import com.example.devgrid.githubgistcommenter.presenter.BasePresenter;
import com.example.devgrid.githubgistcommenter.service.GitHubClient;
import com.example.devgrid.githubgistcommenter.view.qrcodescanner.QrCodeView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by claudio on 14/05/2018.
 */

public class QrCodeScannerPresenter extends BasePresenter<QrCodeView> implements QrCodeScannerInterface {


    public QrCodeScannerPresenter(QrCodeView view) {
        super(view);
    }


    /**
     * Function to check if the Qrcode data is a valid gist id
     *
     * @param readGist
     */

    @Override
    public void validateGist(final String readGist) {
        getView().showProgressDialogValidateGist();

        GitHubClient client = ServiceGenerator.createService(GitHubClient.class);
        Call<Gist> call = client.validateGistt(readGist);

        call.enqueue(new Callback<Gist>() {
            @Override
            public void onResponse(Call<Gist> call, Response<Gist> response) {
                getView().dismissProgress();
                if (response.message().equals("OK")) {
                    GistAccessManager.getInstance().setGistId(readGist);
                    GistAccessManager.getInstance().setTargetGist(response.body());
                    getView().goToGistCommentSecction();
                } else {
                    getView().showInvalidGistToast();
                }
            }

            @Override
            public void onFailure(Call<Gist> call, Throwable t) {
                getView().dismissProgress();
                getView().showToastError();
            }
        });

    }


}
