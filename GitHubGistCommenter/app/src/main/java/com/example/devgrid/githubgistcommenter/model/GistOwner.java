package com.example.devgrid.githubgistcommenter.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by claudio on 15/05/2018.
 */

public class GistOwner {
    @SerializedName("login")
    public String name;

    public String getName() {
        return name;
    }
}
