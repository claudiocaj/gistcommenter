package com.example.devgrid.githubgistcommenter.view.qrcodescanner;

import com.example.devgrid.githubgistcommenter.presenter.qrcodescanner.QrCodeScannerInterface;
import com.example.devgrid.githubgistcommenter.presenter.qrcodescanner.QrCodeScannerPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class QrCodeScannerModule {
    @Provides
    QrCodeScannerInterface provides(QrCodeView view) {
        return new QrCodeScannerPresenter(view);
    }

}
