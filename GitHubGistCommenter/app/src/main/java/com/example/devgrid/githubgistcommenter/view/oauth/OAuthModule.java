package com.example.devgrid.githubgistcommenter.view.oauth;

import com.example.devgrid.githubgistcommenter.presenter.oauth.OAuthInterface;
import com.example.devgrid.githubgistcommenter.presenter.oauth.OAuthPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class OAuthModule {
    @Provides
    OAuthInterface provideOathPresenter(OauthView view) {
        return new OAuthPresenter(view);
    }

}
