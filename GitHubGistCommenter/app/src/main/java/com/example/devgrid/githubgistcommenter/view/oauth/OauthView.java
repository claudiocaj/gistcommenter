package com.example.devgrid.githubgistcommenter.view.oauth;

import com.example.devgrid.githubgistcommenter.view.BaseView;

public interface OauthView extends BaseView {
    public void onRequestOAuthBtnPressed();

    public void goToQrCodeSection();

    public void ahowProgressOathLoading();
}
