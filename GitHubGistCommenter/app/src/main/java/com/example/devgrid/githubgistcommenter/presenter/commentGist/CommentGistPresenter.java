package com.example.devgrid.githubgistcommenter.presenter.commentGist;

import com.example.devgrid.githubgistcommenter.manager.GistAccessManager;
import com.example.devgrid.githubgistcommenter.manager.ServiceGenerator;
import com.example.devgrid.githubgistcommenter.model.GistComment;
import com.example.devgrid.githubgistcommenter.presenter.BasePresenter;
import com.example.devgrid.githubgistcommenter.service.GitHubClient;
import com.example.devgrid.githubgistcommenter.view.commentgist.CommentGistView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by claudio on 14/05/2018.
 */

public class CommentGistPresenter extends BasePresenter<CommentGistView> implements CommentGistInterface {


    public CommentGistPresenter(CommentGistView view) {
        super(view);
    }

    /**
     * Send the comment for the specific gist
     */
    @Override
    public void sendComment(String commentString) {
        getView().showProgressCommentingGist();
        GistComment comment = new GistComment(commentString);
        GitHubClient client = ServiceGenerator.createService(GitHubClient.class);
        Call<ResponseBody> call = client.commentAGist(GistAccessManager.getInstance().getGistId(), comment, "token " + GistAccessManager.getInstance().getAccessToken());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                getView().dismissProgress();
                getView().showToastGistCommented();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                getView().dismissProgress();
                getView().showToastError();
            }
        });
    }
}
