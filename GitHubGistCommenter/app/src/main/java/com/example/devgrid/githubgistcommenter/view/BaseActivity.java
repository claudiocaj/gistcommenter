package com.example.devgrid.githubgistcommenter.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.devgrid.githubgistcommenter.R;

/**
 * Created by claudio on 15/05/2018.
 */

public abstract class BaseActivity extends Activity implements BaseView {

    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Show a progress dialog to the user
     *
     * @param msg the message on the progress dialog
     */
    @Override
    public void showProgress(String msg) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            dismissProgress();
        mProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.app_name), msg);
    }

    /**
     * dismiss the progress dialog that is running.
     */
    @Override
    public void dismissProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * Show a toast to the user
     *
     * @param msg the message that the toast will show
     */
    @Override
    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showToastError() {
        showToast(getResources().getString(R.string.request_error));
    }
}
