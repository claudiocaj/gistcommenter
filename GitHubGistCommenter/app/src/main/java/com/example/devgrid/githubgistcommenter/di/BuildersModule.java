package com.example.devgrid.githubgistcommenter.di;

import com.example.devgrid.githubgistcommenter.view.commentgist.CommentGistActivity;
import com.example.devgrid.githubgistcommenter.view.commentgist.CommentGistModule;
import com.example.devgrid.githubgistcommenter.view.commentgist.CommentGistViewModule;
import com.example.devgrid.githubgistcommenter.view.oauth.OAuthActivity;
import com.example.devgrid.githubgistcommenter.view.oauth.OAuthModule;
import com.example.devgrid.githubgistcommenter.view.oauth.OAuthViewModule;
import com.example.devgrid.githubgistcommenter.view.qrcodescanner.QrCodeScannerActivity;
import com.example.devgrid.githubgistcommenter.view.qrcodescanner.QrCodeScannerModule;
import com.example.devgrid.githubgistcommenter.view.qrcodescanner.QrCodeViewModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {
    @ContributesAndroidInjector(modules = {OAuthViewModule.class, OAuthModule.class})
    abstract OAuthActivity bindOauthActivity();

    @ContributesAndroidInjector(modules = {QrCodeViewModule.class, QrCodeScannerModule.class})
    abstract QrCodeScannerActivity bindOQrCodeActivity();

    @ContributesAndroidInjector(modules = {CommentGistViewModule.class, CommentGistModule.class})
    abstract CommentGistActivity bindCommentGistActivity();


}
